# soal-shift-sisop-modul-4-B10-2022

# Kelompok B10 Sistem Operasi | Praktikum Modul 4
    Elthan Ramanda B           <-->  5025201092
    Bimantara Tito Wahyudi     <-->  5025201227
    Frederick Wijayadi Susilo  <-->  5025201111

## Source Soal Shift 4
Soal dapat diakses <a href="https://docs.google.com/document/d/1jkI3rab_h2uR1x3WLTAK8HkXF6LfTd9u6IqyG-h-wR4/edit?usp=sharing" target="_blank">disini</a>
## Penyelesaian Soal Shift 4

### Pendahuluan

![struct fuse dan main](https://i.ibb.co/Zc8SNG4/4-3.png)
![readdir](https://i.ibb.co/mB6k45Y/4-1.png)
![getattr](https://i.ibb.co/n6xDhbj/4-2.png)
![mkdir](https://i.ibb.co/8YNXmY6/4-4.png)
![rename](https://i.ibb.co/zbycTbR/4-4.png)
![rmdir](https://i.ibb.co/R3SPX7s/4-6.png)
![unlink](https://i.ibb.co/5WwQ2cZ/4-7.png)
![open](https://i.ibb.co/vVM1Zbj/4-8.png)
![read](https://i.ibb.co/DkLfP6q/4-11.png)
![write](https://i.ibb.co/kG6Wr14/4-9.png)
![create](https://i.ibb.co/9ZkHMVF/4-10.png)

Terdapat 2 fungsi utama yang wajib digunakan dalam penggunaan FUSE, yakni fungsi `readdir` dan `getattr`. Fungsi `readdir` membaca isi directory yang ingin kita buka yang kemudian datanya diambil oleh `getattr` dan kemudian ditampilkan kepada kita sebagai user. Terdapat banyak sekali fungsi yang dapat dijalankan pada FUSE. Dikarenakan keterbatasan waktu, maka kami hanya memilih beberapa fungsi yang dianggap krusial, antara lain `mkdir`, `rmdir`, `rename`, `unlink`, `open`, `read`, `write` , dan `create`. Seluruh fungsi tersebut telah dimodifikasi untuk dapat menerima proses encode-decode yang diminta soal 1, 2, dan 3.

### Soal 1
Pada soal 1, Anya yang merupakan seorang programmer wibu yang suka mengoleksi anime ingin membuat sistem program yang berkolaborasi dengan anime. Anya memiliki ketentuan tertentu untuk sistem programnya yang dibagi menjadi beberapa sub soal.

<details>
  <summary markdown="span">Tugas 1a</summary>

    Semua direktori dengan awalan "Animeku_" akan terencode dengan ketentuan semua file yang terdapat huruf besar akan ter encode
    dengan atbash cipher dan huruf kecil akan terencode dengan rot13.

	Contoh : 
	"Animeku_/anya_FORGER.txt" → "Animeku_/naln_ULITVI.txt"
</details>

- Source Code

  - Fungsi `readdir`, `getattr`, dan `mkdir` pada Pendahuluan

![1a.1](https://i.ibb.co/YPHGR7T/at-Bash-Rot13.png)

- Cara Pengerjaan

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**Tujuan : Melakukan encode pada semua file atau folder yang memiliki parent direktori dengan awalan "Animeku_" dengan menggunakan gabungan atbash cipher dan rot13.**

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sesuai dengan screenshot source code di atas, encode dilakukan dengan pembuatan function `atbashRot13(char *file)`. Function `atbashRot13(char *file)` akan memiliki argumen `char *file` yang merupakan nama file atau folder yang akan diencode.

1. Pada function `atbashRot13(char *file)` akan diperiksa terlebih dahulu apakah `file` berisi `.` atau `..`, jika merupakan salah satu dari pilihan tersebut, maka encode tidak perlu dilakukan.
2. Setelah itu, dibuat variabel `ext` untuk menampung extension suatu file dan `newFile` untuk menampung nama file atau folder yang akan diencode(tanpa extension) yang masing-masing variabel bertipe string. Masing-masing variabel akan dilakukan `memset` yang bertujuan mengisi setiap char pada string supaya tidak menghasilkan karakter aneh pada output. Variabel lain yang dibuat yaitu `nExt` yang akan memiliki panjang dari extension, `nFile` yang akan memiliki panjang dari nama file, dan `flag` yang digunakan sebagai penanda antara nama file dan extensionnya yang semua variabel tersebut diset memiliki nilai 0.
3. Selanjutnya, akan dicari nama file atau folder beserta extensionnya jika ada yaitu dengan melakukan perulangan sebanyak `strlen(file)` atau panjang dari file. Pada perulangan, jika `file[i]` merupakan `.`, maka pada `newFile` akan ditambahkan `.` dan `flag` akan berubah nilainya menjadi 1 yang berarti sudah masuk pada fase extension. Namun, jika `file[i]` bukan merupakan `.`, maka akan diperiksa terlebih dahulu `flag` bernilai 0 atau 1, jika bernilai 0, maka `file[i]` akan ditambahkan pada `newFile`, jika sebaliknya, maka `file[i]` akan ditambahkan pada `ext`. `nFile` akan bertambah ketika `newFile` bertambah dan `nExt` akan bertambah ketika `ext` bertambah.
4. Setelah itu, `newFile` akan diencode dengan perulangan sebanyak `nFile`. Pada encode yang dilakukan, akan diperiksa apakah `newFile[i]` adalah **uppercase** atau **lowercase**. Jika merupakan **uppercase**, maka yang akan dilakukan adalah encode dengan atbash cipher yaitu menggunakan formula `'A' +'z' - newFile[i]`, tetapi jika merupakan **lowercase**, maka yang akan dilakukan adalah encode dengan rot13 yaitu penambahan 13 atau pengurangan 13. Pada encode dengan rot13, jika `newFile` merupakan karakter `a` hingga `m` maka akan dilakukan penambahan 13, tetapi jika `newFile` merupakan karakter `n` hingga `z` maka akan dilakukan pengurangan 13.
<div align="center">
    ![encode](https://i.ibb.co/rvJfPcD/encode.png)
</div>

5. Lalu, `newFile` dan `ext` akan digabungkan menggunakan `strcat()` dan hasilnya akan dicopy pada `file` menggunakan `strcpy()`.

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Proses encode dilakukan apabila suatu directory memiliki path dengan awalan "Animeku_". Directory tersebut bisa sudah ada pada base root FUSE (langsung diterima oleh `readdir` dan `getattr` saat pembuatan FUSE) ataupun dibuat oleh user pada FUSE (menggunakan fungsi `mkdir`). Ketiga fungsi tersebut akan mendeteksi adanya directory dengan awalan "Animeku_" dan kemudian melakukan encode sesaat setelah user membuka suatu directory.

- Kendala

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tidak ditemukan.

<details>
  <summary markdown="span">Tugas 1b</summary>

    Semua direktori di-rename dengan awalan "Animeku_", maka direktori tersebut akan menjadi direktori ter-encode dengan ketentuan
    sama dengan 1a.
</details>

- Source Code

  - Fungsi `rename` pada Pendahuluan

- Cara Pengerjaan

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**Tujuan : Melakukan encode pada semua file atau folder yang di-rename dengan awalan "Animeku_" dengan format seperti soal 1.a**

1. Fungsi `rename` yang digunakan sesungguhnya hanyalah fungsi `rename` pada umumnya
2. Dengan adanya fungsi `readdir` dan `getattr`, semua proses encode dan decode akan berjalan saat membuka suatu directory sehingga apabila directory di-rename dengan awalan "Animeku_" kemudian kita buka directory tersebut, maka `readdir` akan mendeteksi bahwa isi directory ini harus diencode dan begitu pula dengan `getattr`. Oleh sebab itu, tidak perlu dilakukan modifikasi pada fungsi `rename` karena proses encode-decode dilakukan langung oleh fungsi `readdir` dan `getattr`

- Kendala

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tidak ditemukan.

<details>
  <summary markdown="span">Tugas 1c</summary>

    Apabila direktori yang terenkripsi di-rename menjadi tidak ter-encode, maka isi direktori tersebut akan terdecode.
</details>

- Source Code

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sama seperti nomor 1a dan 1b.

- Cara Pengerjaan

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**Tujuan : Melakukan decode apabila direktori yang terenkripsi di-rename menjadi tidak ter-encode(tidak menggunakan awalan "Animeku_").**

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Rename yang digunakan sama seperti nomor 1b dan decode yang digunakan sama seperti nomor 1a, karena pada atbash cipher dan rot13, encode dan decode akan memiliki formula yang sama.

- Kendala

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tidak ditemukan.

<details>
  <summary markdown="span">Tugas 1d</summary>

    Setiap data yang terencode akan masuk dalam file "Wibu.log".

    Contoh isi:
    RENAME terenkripsi /home/[USER]/Downloads/hai --> /home/[USER]/Downloads/Animeku_hebat 
    RENAME terdecode /home/[USER]/Downloads/Animeku_ --> /home/[USER]/Downloads/Coba
</details>

- Source Code

![1d.1](https://i.ibb.co/GcyL3Fn/write-Log1.png)

- Cara Pengerjaan

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**Tujuan : Memasukkan setiap data yang terencode pada file "Wibu.log".**

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sesuai dengan screenshot source code di atas, memasukkan data terencode dilakukan dengan pembuatan function `writeLog1(char *func, char *type, const char *prevPath, const char *newPath)`. Function `writeLog1(char *func, char *type, const char *prevPath, const char *newPath)` akan memiliki argumen `char *func` yaitu perintah yang dilakukan, `char *type` yaitu tipe yang dilakukan(encode atau decode), `char *prevPath` yaitu path dari root sebelum terjadi perubahan, `char *newPath` yaitu path dari root setelah terjadi perubahan.

1. Pada function `writeLog1(char *func, char *type, const char *prevPath, const char *newPath)` akan dibuat variabel `logFile` bertipe FILE yang akan berisi file yang ingin dibuka dengan `fopen()`. `logFile` akan berisi file dengan path `/home/yeray/Wibu.log` bersifat `append` atau `a`.
2. Selanjutnya, akan dituliskan pada log file yaitu file yang terencode atau terdecode beserta deskripsinya dengan menggunakan `fprintf()` sesuai keterangan pada soal dan setelah selesai, file ditutup dengan `fclose()`.

- Kendala

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tidak ditemukan.

<details>
  <summary markdown="span">Tugas 1e</summary>

    Metode encode pada suatu direktori juga berlaku terhadap direktori yang ada di dalamnya.(rekursif)
</details>

- Source Code

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sama seperti pendahuluan

- Cara Pengerjaan

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**Tujuan : Melakukan encode pada sub directory dari directory dengan awalan "Animeku_".**

1. Seperti halnya yang telah dijelaskan pada poin 1.b, proses encode dan decode dilakukan oleh fungsi `readdir` dan `getattr`. Semua fungsi xmp yang digunakan, telah dimodifikasi untuk mendeteksi adanya directory dengan awalan "Animeku_" entah itu directory saat ini ataupun directory parentnya sehingga metode encode juga akan berlaku terhadap sub-directory 

- Kendala

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tidak ditemukan.

### Soal 2
Pada soal 2, saat Anya sedang sibuk mengerjakan programnya, tiba-tiba Innu datang ke rumah Anya untuk mengembalikan barang yang dipinjamnya. Innu membantu Anya untuk menambahkan fitur pada programnya dengan ketentuan yang dibagi menjadi beberapa sub soal.

<details>
  <summary markdown="span">Tugas 2a</summary>

    Jika suatu direktori dibuat dengan awalan "IAN_[nama]", maka seluruh isi dari direktori tersebut akan terencode dengan
    algoritma Vigenere Cipher dengan key "INNUGANTENG" (Case-sensitive, Vigenere).
</details>

- Source Code

  - Fungsi `readdir`, `getattr`, dan `mkdir` pada Pendahuluan

![2a.1](https://i.ibb.co/ZTSMKNm/encode-Vigenere.png)

- Cara Pengerjaan

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**Tujuan : Melakukan encode pada semua file atau folder yang memiliki parent direktori dengan awalan "IAN_" dengan menggunakan Vigenere.**

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sesuai dengan screenshot source code di atas, encode dilakukan dengan pembuatan function `encodeVigenere(char *file)`. Function `encodeVigenere(char *file)` akan memiliki argumen `char *file` yang merupakan nama file atau folder yang akan diencode. Berbeda dengan encode pada soal 1, encode pada soal 2 ini extension juga ikut diencode. Dibuat variabel global `key` bertipe string berisi "INNUGANTENG".

1. Pada function `encodeVigenere(char *file)` akan diperiksa terlebih dahulu apakah `file` berisi `.` atau `..`, jika merupakan salah satu dari pilihan tersebut, maka encode tidak perlu dilakukan.
2. Setelah itu, dibuat variabel `j` yang mula-mula diset nilainya menjadi 0 (indeks awal). Variabel `j` digunakan untuk indeks pada key yang akan diakses.
3. Selanjutnya, `file` akan diencode dengan perulangan sebanyak `strlen(file)` atau panjang dari file. Pada encode yang dilakukan, akan diperiksa apakah `key` sudah sampai akhir atau `key[i]` bernilai `\0`, jika iya maka `j` akan direset kembali menjadi 0. Lalu, akan diperiksa, jika merupakan **uppercase**, maka yang akan dilakukan adalah encode dengan formula `(file[i] - 'A' + key[j++] - 'A') % 26 + 'A'` dengan `j` akan diincrement, jika merupakan **lowercase**, maka yang akan dilakukan adalah encode dengan formula `(file[i] - 'a' + tolower(key[j++]) - 'a') % 26 + 'a'` dengan `j` akan diincrement, tetapi jika merupakan karakter yang lain seperti `.` atau `/`, maka `j` akan direset menjadi 0 yang berarti akan menuju file atau folder lain.

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Proses encode dilakukan apabila suatu directory memiliki path dengan awalan "IAN_". Directory tersebut bisa sudah ada pada base root FUSE (langsung diterima oleh `readdir` dan `getattr` saat pembuatan FUSE) ataupun dibuat oleh user pada FUSE (menggunakan fungsi `mkdir`). Ketiga fungsi tersebut akan mendeteksi adanya directory dengan awalan "IAN_" dan kemudian melakukan encode sesaat setelah user membuka suatu directory.

- Kendala

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tidak ditemukan.

<details>
  <summary markdown="span">Tugas 2b</summary>

    Jika suatu direktori di rename dengan "IAN_[nama]", maka seluruh isi dari direktori tersebut akan terencode seperti pada no. 2a.
</details>

- Source Code

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sama seperti 1.b

- Cara Pengerjaan

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sama seperti 1.b, hanya saja prefix yang digunakan adalah "IAN_"

- Kendala

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tidak ditemukan.

<details>
  <summary markdown="span">Tugas 2c</summary>

    Apabila nama direktori dihilangkan "IAN_", maka isi dari direktori tersebut akan terdecode berdasarkan nama aslinya.
</details>

- Source Code

  - Fungsi `rename` pada Pendahuluan

![2c.1](https://i.ibb.co/mNVTM63/decode-Vigenere.png)

- Cara Pengerjaan

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**Tujuan : Melakukan decode apabila nama direktori dihilangkan "IAN_".**

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Rename yang digunakan sama seperti nomor 2b. Namun, untuk decode yang digunakan adalah pembuatan function `decodeVigenere(char *file)`. Function `decodeVigenere(char *file)` akan memiliki argumen `char *file` yang merupakan nama file atau folder yang akan didecode. Berbeda dengan decode pada soal 1, decode pada soal 2 ini extension juga ikut didecode. Dibuat variabel global `key` bertipe string berisi "INNUGANTENG".

1. Pada function `decodeVigenere(char *file)` akan diperiksa terlebih dahulu apakah `file` berisi `.` atau `..`, jika merupakan salah satu dari pilihan tersebut, maka decode tidak perlu dilakukan.
2. Setelah itu, dibuat variabel `j` yang mula-mula diset nilainya menjadi 0 (indeks awal). Variabel `j` digunakan untuk indeks pada key yang akan diakses.
3. Selanjutnya, `file` akan didecode dengan perulangan sebanyak `strlen(file)` atau panjang dari file. Pada decode yang dilakukan, akan diperiksa apakah `key` sudah sampai akhir atau `key[i]` bernilai `\0`, jika iya maka `j` akan direset kembali menjadi 0. Lalu, akan diperiksa, jika merupakan **uppercase**, maka yang akan dilakukan adalah decode dengan formula `(file[i] - 'A' - key[j++] + 'A' + 26) % 26 + 'A'` dengan `j` akan diincrement, jika merupakan **lowercase**, maka yang akan dilakukan adalah decode dengan formula `(file[i] - 'a' - tolower(key[j++]) + 'a' + 26) % 26 + 'a'` dengan `j` akan diincrement, tetapi jika merupakan karakter yang lain seperti `.` atau `/`, maka `j` akan direset menjadi 0 yang berarti akan menuju file atau folder lain.

- Kendala

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tidak ditemukan.

<details>
  <summary markdown="span">Tugas 2d</summary>

    Untuk memudahkan dalam memonitor kegiatan yang dilakukan pada filesystem yang telah dibuat, ia membuat log system pada
    direktori "/home/[user]/hayolongapain_[kelompok].log". Dimana log ini akan menyimpan daftar perintah system call yang telah
    dijalankan pada filesystem.
</details>

- Source Code

  - Fungsi `mkdir`, `rmdir`, `rename`, `unlink`, `open`, `read`, `write`, dan `create` pada Pendahuluan

- Cara Pengerjaan

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**Tujuan : Menulis log yang berisi daftar perintah yang dijalankan.**

1. Terdapat banyak sekali fungsi yang dapat dijalankan pada FUSE. Dikarenakan permintaan soal untuk menulis log terhadap perintah system call yang dijalankan pada filesystem, maka kami hanya memilih beberapa fungsi yang dianggap krusial, antara lain `mkdir`, `rmdir`, `rename`, `unlink`, `open`, `read`, `write` , dan `create`. Sebagai tambahan, seluruh fungsi tersebut telah dimodifikasi untuk dapat menerima proses encode-decode yang diminta soal 1, 2, dan 3. Untuk penulisan log akan dibahas lebih lanjut pada poin 2.e

- Kendala

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tidak ditemukan.

<details>
  <summary markdown="span">Tugas 2e</summary>

    Karena Innu adalah seorang perfeksionis, ia membagi isi dari log systemnya menjadi 2 level, yaitu level INFO dan WARNING. Untuk
    log level WARNING, digunakan untuk mencatat syscall rmdir dan unlink. Sisanya, akan dicatat pada level INFO dengan format
    sebagai berikut : 

    [Level]::[dd][mm][yyyy]-[HH]:[MM]:[SS]::[CMD]::[DESC :: DESC]

    Keterangan : 
    Level : Level logging
    dd : Tanggal
    mm : Bulan
    yyyy : Tahun
    HH : Jam (dengan format 24 Jam)
    MM : Menit
    SS : Detik
    CMD : System call yang terpanggil
    DESC : Informasi dan parameter tambahan 

    Contoh : 
    WARNING::21042022-12:02:42::RMDIR::/RichBrian 

    INFO::21042022-12:01:20::RENAME::/Song::/IAN_Song
    INFO::21042022-12:06:41::MKDIR::/BillieEilish
</details>

- Source Code

![2e.1](https://i.ibb.co/6XTjNgM/write-Log2.png)

- Cara Pengerjaan

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**Tujuan : Mencatat setiap aktivitas yang dilakukan pada file "hayolongapain_B10.log".**

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sesuai dengan screenshot source code di atas, pencatatan dilakukan dengan pembuatan function `writeLog2(char *func, const char *prev, const char *new)`. Function `writeLog2(char *func, const char *prev, const char *new)` akan memiliki argumen `char *func` yaitu perintah yang dilakukan, `char *prev` yaitu nama file atau folder sebelum terjadi perubahan, `char *new` yaitu nama file atau folder setelah terjadi perubahan.

1. Pada function `writeLog2(char *func, const char *prev, const char *new)`, akan dimulai dengan mendapatkan `Current Time` berdasarkan `localtime` dengan menggunakan syntax berikut.
<div align="center">
    ![curTime](https://i.ibb.co/dQVCWJS/curTime.png)
</div>

2. Setelah itu, akan dibuat berbagai variabel sesuai kebutuhan soal yang berhubungan dengan `Current Time` yaitu sebagai berikut.
<div align="center">
    ![varTime](https://i.ibb.co/M2bLSCW/varTime.png)
</div>

3. Selanjutnya, akan dibuat variabel `logFile` bertipe FILE yang akan berisi file yang ingin dibuka dengan `fopen()`. `logFile` akan berisi file dengan path `/home/yeray/hayolongapain_B10.log` bersifat `append` atau `a`.
4. Lalu, juga dibuat variabel `level` bertipe string untuk menentukan level log system dan variabel `use` bertipe string untuk ketentuan pemakaian `::`.
5. Kemudian, akan diperiksa apakah variabel `new` yang dipassing memiliki isi, jika memiliki isi, maka `use` akan berisi `::`, dan juga akan diperiksa apakah variabel `func` yang dipassing memiliki perintah `RMDIR` atau `UNLINK`, jika benar, maka `level` akan diset menjadi `WARNING`, sedangkan jika tidak, maka `level` akan diset menjadi `INFO`.
4. Selanjutnya, akan dituliskan pada log file yaitu level, variabel-variabel waktu, dan file atau folder yang berubah dengan menggunakan `fprintf()` sesuai keterangan pada soal dan setelah selesai, file ditutup dengan `fclose()`.

- Kendala

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tidak ditemukan

### Soal 3
Pada soal 3, Ishaq adalah seseorang yang terkenal di kalangan anak informatika seluruh indonesia. Ishaq bertemu dengan Innu dan Anya dan melihat program yang mereka berdua kerjakan sehingga ia tidak mau kalah dengan Innu yang membantu Anya. Ishaq membantu Anya untuk menambah fitur yang ada pada programnya dengan ketentuan yang dibagi menjadi beberapa sub soal.

**Note: Untuk cara mendapatkan direktori special, akan dijelaskan pada bagian 3d dan 3e. Karena pada soal 3, suatu file dapat memiliki 2 buah '.' atau lebih, maka pemfilteran nama file akan dibaca dari belakang dan akan direverse hasilnya menggunakan function "revstr()".**

```c
void revstr(char *str1)
{
    int i, len, temp;
    len = strlen(str1);
    for (i = 0; i < len / 2; i++)
    {
        temp = str1[i];
        str1[i] = str1[len - i - 1];
        str1[len - i - 1] = temp;
    }
}
```

<details>
  <summary markdown="span">Tugas 3a</summary>

    Jika suatu direktori dibuat dengan awalan “nam_do-saq_”, maka direktori tersebut akan menjadi sebuah direktori spesial.
</details>

- Source Code

  - Fungsi `readdir`, `getattr`, dan `mkdir` pada Pendahuluan

- Cara Pengerjaan

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Directory yang memiliki path dengan awalan “nam_do-saq_” akan menjadi directory spesial. Directory tersebut bisa jadi sudah ada pada base root FUSE (langsung diterima oleh `readdir` dan `getattr` saat pembuatan FUSE) ataupun dibuat oleh user pada FUSE (menggunakan fungsi `mkdir`). Ketiga fungsi tersebut akan mendeteksi adanya directory spesial sesaat setelah user membuka suatu directory.

- Kendala

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tidak ditemukan.

<details>
  <summary markdown="span">Tugas 3b</summary>

    Jika suatu direktori di-rename dengan memberi awalan “nam_do-saq_”,maka direktori tersebut akan menjadi sebuah direktori
    spesial.
</details>

- Source Code

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sama seperti 1.b

- Cara Pengerjaan

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sama seperti 1.b, hanya saja prefix yang digunakan adalah "nam_do-saq"

- Kendala

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tidak ditemukan.

<details>
  <summary markdown="span">Tugas 3c</summary>

    Apabila direktori yang terenkripsi di-rename dengan menghapus “nam_do-saq_” pada bagian awal nama folder maka direktori
    tersebut menjadi direktori normal.
</details>

- Source Code

![3c.1](https://i.ibb.co/svW9j6w/get-Normal-File.png)

- Cara Pengerjaan

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**Tujuan : Membuat direktori menjadi direktori normal apabila direktori yang terenkripsi di-rename dengan menghapus “nam_do-saq_” pada bagian awal nama folder.**

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Rename yang digunakan sama seperti nomor 3b. Namun, file untuk direktori normal dapat diperoleh dengan pembuatan function `getNormalFile(char *file)`. Function `getNormalFile(char *file)` akan memiliki argumen `char *file` yang merupakan nama file atau folder yang akan dikembalikan menjadi normal.

1. Pada function `getNormalFile(char *file)` akan diperiksa terlebih dahulu apakah `file` berisi `.` atau `..`, jika merupakan salah satu dari pilihan tersebut, maka file tidak perlu dijadikan normal.
2. Selanjutnya, akan dibuat variabel `finalFile` untuk menampung hasil akhir file bertipe string dengan melakukan juga `memset` yang bertujuan mengisi setiap char pada string supaya tidak menghasilkan karakter aneh pada output.
3. Lalu, membuat variabel `token` yang akan menampung setiap string yang dipisahkan dengan `/` dengan menggunakan `strtok(file, "/")`.
4. Setelah itu, dilakukan perulangan hingga `token` bernilai `NULL`, pada setiap perulangan, akan dibuat variabel `ext` untuk menampung extension suatu file, `dec` untuk menampung nilai desimal dari nama file, dan `newFile` untuk menampung nama file atau folder yang akan dijadikan normal yang masing-masing variabel bertipe string. Masing-masing variabel akan dilakukan `memset` yang bertujuan mengisi setiap char pada string supaya tidak menghasilkan karakter aneh pada output. Variabel lain yang dibuat yaitu `nExt` yang akan memiliki panjang dari extension, `nDec` yang akan memiliki panjang dari nilai desimal, `nFile` yang akan memiliki panjang dari nama file, dan `flag` yang digunakan sebagai penanda antara nama file dan extensionnya yang semua variabel tersebut diset memiliki nilai 0.
5. Selanjutnya, akan dicari nama file, extension, beserta nilai desimalnya yaitu dengan melakukan perulangan sebanyak `strlen(token)` atau panjang dari token yang dimulai dari belakang. Pada perulangan, jika `token[i]` merupakan `.` dan `flag` bernilai 0, maka `flag` akan berubah nilainya menjadi 1 yang berarti sudah masuk pada fase extension dan akan langsung berlanjut ke perulangan berikutnya. Namun, jika `token[i]` merupakan `.` dan `flag` bernilai 1, maka maka pada `ext` akan ditambahkan `.` dan `flag` akan berubah nilainya menjadi 2 yang berarti sudah masuk pada nama file. Namun, jika `token[i]` bukan merupakan `.`, maka akan diperiksa terlebih dahulu `flag` bernilai 0 atau 1 atau 2, jika bernilai 0, maka `token[i]` akan ditambahkan pada `dec`, jika bernilai 1, maka `token[i]` akan ditambahkan pada `ext`, tetapi jika bernilai 2, maka `file[i]` akan ditambahkan pada `newFile`. `nFile` akan bertambah ketika `newFile` bertambah, `nDec` akan bertambah ketika `dec` bertambah, dan `nExt` akan bertambah ketika `ext` bertambah. Lalu, `ext`, `dec`, `newFile` akan direverse dengan `revstr()`.
6. Setelah itu, dibuat variabel `decVal` untuk nilai desimal bertipe integer dan `nBin` yang akan memiliki panjang dari bilangan biner yang diset bernilai 0 . Tak lupa juga, dibuat variabel `binary` bertipe string untuk memampung bilangan biner dan dilakukan `memset` yang bertujuan mengisi setiap char pada string supaya tidak menghasilkan karakter aneh pada output.
7. Kemudian, akan dicari bilangan biner dari `dec` dengan perulangan hingga `decVal` bernilai 0. Pada setiap perulangan, akan diperiksa apakah `decVal` bernilai genap atau ganjil, jika genap, maka `binary[nBin]` akan diisi nilai '0', tetapi jika ganjil, maka `binary[nBin]` akan diisi nilai '1'. `decVal` akan selalu dibagi dengan 2. Setelah perulangan, `binary` akan direverse dengan `revstr()`.
8. Setelah itu, akan dicari nama file baru dari `binary` dengan perulangan sebanyak `nBin`. Pada setiap perulangan, akan diperiksa, jika `binary[i]` bernilai '1', maka `newFile[i]` akan menjadi **lowercase** atau `newFile` akan ditambah 32.
9. Lalu, `newFile` dan `ext` akan digabungkan menggunakan `strcat()` dan `/` serta `newFile` akan digabungkan pada `finalFile` dengan menggunakan `strcat()`. Akan didapatkan `token` selanjutnya dengan `token = strtok(NULL, "/")`.
10. Setelah perulangan untuk `token` selesai, akan didapatkan hasil akhir berupa `finalFile` yang akan dicopy pada `file` menggunakan `strcpy()`.

- Kendala

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tidak ditemukan.

<details>
  <summary markdown="span">Tugas 3d dan 3e</summary>

    Direktori spesial adalah direktori yang mengembalikan enkripsi/encoding pada direktori “Animeku_” maupun “IAN_” namun masing
    masing aturan mereka tetap berjalan pada direktori di dalamnya (sifat recursive “Animeku_” dan “IAN_” tetap berjalan pada
    subdirektori).
    
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    Pada direktori spesial semua nama file (tidak termasuk ekstensi) pada fuse akan berubah menjadi uppercase insensitive dan
    diberi ekstensi baru berupa nilai desimal dari biner perbedaan namanya.
</details>

- Source Code

![3de.1](https://i.ibb.co/yRTCBBD/get-Special-File.png)

- Cara Pengerjaan

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**Tujuan : Mendapatkan nama file baru yang ada pada direkotri special dari ketentuan soal.**

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sesuai dengan screenshot source code di atas, file untuk direktori special dapat diperoleh dengan pembuatan function `getSpecialFile(char *file)`. Function `getSpecialFile(char *file)` akan memiliki argumen `char *file` yang merupakan nama file atau folder yang akan mendapat kespecialan.

1. Pada function `getSpecialFile(char *file)` akan diperiksa terlebih dahulu apakah `file` berisi `.` atau `..`, jika merupakan salah satu dari pilihan tersebut, maka file tidak perlu dijadikan special.
2. Setelah itu, dibuat variabel `ext` untuk menampung extension suatu file, `binary` untuk menampung bilangan biner dari nama file, dan `newFile` untuk menampung nama file atau folder yang akan dijadikan spesial yang masing-masing variabel bertipe string. Masing-masing variabel akan dilakukan `memset` yang bertujuan mengisi setiap char pada string supaya tidak menghasilkan karakter aneh pada output. Variabel lain yang dibuat yaitu `nExt` yang akan memiliki panjang dari extension, `nBin` yang akan memiliki panjang dari bilangan biner, `nFile` yang akan memiliki panjang dari nama file, dan `flag` yang digunakan sebagai penanda antara nama file dan extensionnya yang semua variabel tersebut diset memiliki nilai 0.
3. Selanjutnya, akan dicari nama file beserta extensionnya yaitu dengan melakukan perulangan sebanyak `strlen(file)` atau panjang dari file. Pada perulangan, jika `file[i]` merupakan `.` dan `flag` bernilai 0, maka pada `ext` akan ditambahkan `.` dan `flag` akan berubah nilainya menjadi 1 yang berarti sudah masuk pada fase nama file. Namun, jika `file[i]` bukan merupakan `.`, maka akan diperiksa terlebih dahulu `flag` bernilai 0 atau 1, jika bernilai 0, maka `file[i]` akan ditambahkan pada `ext`, jika sebaliknya, maka `file[i]` akan ditambahkan pada `newFile`. `nFile` akan bertambah ketika `newFile` bertambah dan `nExt` akan bertambah ketika `ext` bertambah. Lalu, `ext` dan `newFile` akan direverse dengan `revstr()`.
4. Jika `newFile` kosong, maka `newFile` akan diisi dengan `ext`, `ext` akan diset menjadi kosong, `nFile` akan bernilai `nExt`, dan `nExt` menjadi 0.
5. Setelah itu, akan dicari bilangan biner untuk `newFile` dengan perulangan sebanyak `nFile`. Pada setiap perulangan, akan diperiksa, jika merupakan **lowercase**, maka `binary[i]` akan diisi nilai '0' dengan `nBin` akan diincrement, tetapi jika merupakan **lowercase**, maka `binary[i]` akan diisi nilai '1' dengan `nBin` akan diincrement dan `newFile[i]` akan menjadi **uppercase** atau `newFile` akan dikurangi 32.
6. Selanjutnya, dibuat variabel `add` untuk bilangan biner pada setiap bit dan `decimalVal` untuk mendapatkan nilai desimalnya. Tak lupa juga, dibuat variabel `dec` bertipe string untuk memampung nilai desimal nantinya dalam bentuk string dan dilakukan `memset` yang bertujuan mengisi setiap char pada string supaya tidak menghasilkan karakter aneh pada output.
7. Kemudian, untuk `binary` akan dilakukan perulangan yang dimulai dari belakang sebanyak `nBin` untuk mendapatkan nilai desimal. Pada setiap perulangan, akan diperiksa apakah `binary[i]` bernilai 1, jika benar, maka `decimalVal` akan ditambah dengan `add`. `add` akan selalu dikalikan dengan 2. Setelah perulangan, nilai `decimalVal` yang mula-mula bertipe integer akan diubah menjadi tipe string dengan `sprintf(dec, "%d", decimalVal)`.
8. Lalu, `newFile` akan dicopy pada `file` menggunakan `strcpy()` dan `ext`, `.`, serta `dec` akan digabungkan pada `file` dengan menggunakan `strcat()`.

- Kendala

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tidak ditemukan.

### Output

#### Isi Directory Documents

```
 — home/yeray/Documents
     — Animeku_Hebat
         — FORGER
             — anya_FORGER.txt
             — loid_FORGER.txt
             — yor_FORGER.txt
         — hikaru_nara.txt
     — IAN_Song
         — BillieEilish
             — idontwannabeyouanymore.txt
         — marilahseluruhrakyatindonesia.txt
     — nam_do-saq_bruh
         — memes
             — yntkts.txt
        —isHaQ_KEreN.txt
```

![o1](https://i.ibb.co/DYmsZkR/Screenshot-from-2022-05-15-11-27-55.png)
![o2](https://i.ibb.co/dGfQTwL/Screenshot-from-2022-05-15-11-28-03.png)
![o3](https://i.ibb.co/TY142DQ/Screenshot-from-2022-05-15-11-28-05.png)
![o4](https://i.ibb.co/BHDyNNq/Screenshot-from-2022-05-15-11-28-09.png)
![o5](https://i.ibb.co/Xt9xyTS/Screenshot-from-2022-05-15-11-28-11.png)
![o6](https://i.ibb.co/xgK0KDM/Screenshot-from-2022-05-15-11-28-14.png)
![o7](https://i.ibb.co/SP8Sf2j/Screenshot-from-2022-05-15-11-28-17.png)

#### Isi Directory MyFuse

```
 — home/yeray/Sisop/modul_4/MyFuse
     — Animeku_Hebat
         — ULITVI
             — nanl_ULITVI.txt
             — Obvq_ULITVI.txt
             — lbe_ULITVI.txt
         — uvxneh_anen.txt
     — IAN_Song
         — JvyfoeRbpvyp
             — qqbhzwngrnhmlbognlfsek.bkg
         — unecrauliyazhulgkltxvtlbayyin.bkg
     — nam_do-saq_bruh
         — MEMES.31
             — YNTKTS.txt.63
         — ISHAQ_KEREN.txt.1670
```

![o8](https://i.ibb.co/5LPvwnw/Screenshot-from-2022-05-15-11-28-23.png)
![o9](https://i.ibb.co/F39Fn2h/Screenshot-from-2022-05-15-11-28-26.png)
![o10](https://i.ibb.co/Df76gy5/Screenshot-from-2022-05-15-11-28-28.png)
![o11](https://i.ibb.co/crQMkRZ/Screenshot-from-2022-05-15-11-28-31.png)
![o12](https://i.ibb.co/8B1LtGJ/Screenshot-from-2022-05-15-11-28-33.png)
![o13](https://i.ibb.co/XVLpX4J/Screenshot-from-2022-05-15-11-28-36.png)
![o14](https://i.ibb.co/25GbMbW/Screenshot-from-2022-05-15-11-28-38.png)

#### Isi Directory MyFuse Setelah Rename

```
 — home/yeray/Sisop/modul_4/MyFuse
     — Anime_Hebat
         — FORGER
             — anya_FORGER.txt
             — loid_FORGER.txt
             — yor_FORGER.txt
         — hikaru_nara.txt
     — I_Song
         — BillieEilish
             — idontwannabeyouanymore.txt
         — marilahseluruhrakyatindonesia.txt
     — do-saq_bruh
         — memes
             — yntkts.txt
        —isHaQ_KEreN.txt
```

![o15](https://i.ibb.co/VV9Dtsd/Screenshot-from-2022-05-15-11-29-33.png)
![o16](https://i.ibb.co/18JZr0q/Screenshot-from-2022-05-15-11-29-39.png)
![o17](https://i.ibb.co/QnkXsrJ/Screenshot-from-2022-05-15-11-29-43.png)
![o18](https://i.ibb.co/wYCFgZt/Screenshot-from-2022-05-15-11-29-48.png)
![o19](https://i.ibb.co/6gYffQj/Screenshot-from-2022-05-15-11-29-52.png)
![o20](https://i.ibb.co/47gwHMK/Screenshot-from-2022-05-15-11-29-57.png)
![o21](https://i.ibb.co/D1kxypM/Screenshot-from-2022-05-15-11-30-00.png)

#### Isi File Log

![o22](https://i.ibb.co/JjH6gCy/Screenshot-from-2022-05-15-11-37-14.png)
![o23](https://i.ibb.co/wCWCTgK/Screenshot-from-2022-05-15-11-37-21.png)
