#define FUSE_USE_VERSION 28
#include <fuse.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <sys/time.h>
#include <sys/wait.h>
#include <ctype.h>
static const char *dirPath = "/home/yeray/Documents";
char pref1[20] = "/Animeku_";
char pref2[20] = "/IAN_";
char pref3[20] = "/nam_do-saq_";
char key[12] = "INNUGANTENG";
void writeLog1(char *func, char *type, const char *prevPath, const char *newPath)
{
    FILE *logFile = fopen("/home/yeray/Wibu.log", "a");
    fprintf(logFile, "%s\t%s\t%s\t-->\t%s\n", func, type, prevPath, newPath);
    fclose(logFile);
}
void writeLog2(char *func, const char *prev, const char *new)
{
    time_t curTime;
    struct tm *time_info;
    time(&curTime);
    time_info = localtime(&curTime);
    int year = time_info->tm_year + 1900;
    int month = time_info->tm_mon + 1;
    int day = time_info->tm_mday;
    int hour = time_info->tm_hour;
    int min = time_info->tm_min;
    int sec = time_info->tm_sec;
    FILE *logFile;
    logFile = fopen("/home/yeray/hayolongapain_B10.log", "a");
    char level[10];
    char use[3] = "";
    if (strcmp(new, "") != 0)
        strcpy(use, "::");
    if (strcmp(func, "RMDIR") == 0 || strcmp(func, "UNLINK") == 0)
        strcpy(level, "WARNING");
    else
        strcpy(level, "INFO");
    fprintf(logFile, "%s::%.2d%.2d%d-%.2d:%.2d:%.2d::%s::%s%s%s\n", level, day, month, year, hour, min, sec, func, prev, use, new);
    fclose(logFile);
    return;
}
void atbashRot13(char *file)
{
    if (!strcmp(file, ".") || !strcmp(file, ".."))
        return;
    char ext[10], newFile[10000];
    memset(ext, 0, sizeof(ext));
    memset(newFile, 0, sizeof(newFile));
    int nExt = 0, nFile = 0, flag = 0;
    // get file name and ext
    for (int i = 0; i < strlen(file); i++)
    {
        if (file[i] == '.')
        {
            newFile[nFile++] = file[i];
            flag = 1;
            continue;
        }
        if (flag == 1)
            ext[nExt++] = file[i];
        else
            newFile[nFile++] = file[i];
    }
    // encode or decode file name
    for (int i = 0; i < nFile; i++)
    {
        if (newFile[i] >= 65 && newFile[i] <= 90)
        {
            newFile[i] = 'A' + 'Z' - newFile[i];
        }
        else if (newFile[i] >= 97 && newFile[i] <= 122)
        {
            if (newFile[i] >= 97 && newFile[i] <= 109)
                newFile[i] += 13;
            else
                newFile[i] -= 13;
        }
    }
    // combine file name and ext
    strcat(newFile, ext);
    strcpy(file, newFile);
    printf("rot %s\n", file);
}
void encodeVigenere(char *file)
{
    if (!strcmp(file, ".") || !strcmp(file, ".."))
        return;
    int i, j = 0;
    // encode file
    for (i = 0; i < strlen(file); i++)
    {
        if (key[j] == '\0')
        {
            j = 0;
        }
        if (file[i] >= 65 && file[i] <= 90)
        {
            file[i] = (file[i] - 'A' + key[j++] - 'A') % 26 + 'A';
        }
        else if (file[i] >= 97 && file[i] <= 122)
        {
            file[i] = (file[i] - 'a' + tolower(key[j++]) - 'a') % 26 + 'a';
        }
        else
        {
            j = 0;
        }
    }
}
void decodeVigenere(char *file)
{
    if (!strcmp(file, ".") || !strcmp(file, ".."))
        return;
    int i, j = 0;
    // decode file
    for (i = 0; i < strlen(file); i++)
    {
        if (key[j] == '\0')
        {
            j = 0;
        }
        if (file[i] >= 65 && file[i] <= 90)
        {
            file[i] = (file[i] - 'A' - key[j++] + 'A' + 26) % 26 + 'A';
        }
        else if (file[i] >= 97 && file[i] <= 122)
        {
            file[i] = (file[i] - 'a' - tolower(key[j++]) + 'a' + 26) % 26 + 'a';
        }
        else
        {
            j = 0;
        }
    }
}

void revstr(char *str1)
{
    int i, len, temp;
    len = strlen(str1);
    for (i = 0; i < len / 2; i++)
    {
        temp = str1[i];
        str1[i] = str1[len - i - 1];
        str1[len - i - 1] = temp;
    }
}
void getSpecialFile(char *file)
{
    if (!strcmp(file, ".") || !strcmp(file, ".."))
        return;
    char ext[1000], binary[1000], newFile[1000];
    memset(ext, 0, sizeof(ext));
    memset(binary, 0, sizeof(binary));
    memset(newFile, 0, sizeof(newFile));
    int nExt = 0, nFile = 0, nBin = 0, flag = 0;
    // get file name and ext
    strcpy(newFile, "");
    for (int i = strlen(file) - 1; i >= 0; i--)
    {
        if (file[i] == '.' && flag == 0)
        {
            ext[nExt++] = file[i];
            flag = 1;
            continue;
        }
        if (flag == 1)
            newFile[nFile++] = file[i];
        else
            ext[nExt++] = file[i];
    }
    // printf("%d <-> %d\n", nFile, nExt);
    revstr(ext);
    revstr(newFile);
    if (!strcmp(newFile, ""))
    {
        memset(newFile, 0, sizeof(newFile));
        strcpy(newFile, ext);
        memset(ext, 0, sizeof(ext));
        nFile = nExt;
        nExt = 0;
    }
    printf("%s <-> %s\n", ext, newFile);
    // set new file and binary code
    for (int i = 0; i < nFile; i++)
    {
        if (newFile[i] >= 97 && newFile[i] <= 122)
        {
            binary[nBin++] = '1';
            newFile[i] -= 32;
        }
        else
        {
            binary[nBin++] = '0';
        }
    }
    // convert binary to decimal
    int add = 1, decimalVal = 0;
    char dec[1000];
    memset(dec, 0, sizeof(dec));
    for (int i = nBin - 1; i >= 0; i--)
    {
        if (binary[i] == '1')
            decimalVal += add;
        add *= 2;
    }
    // printf("%s\n", binary);
    sprintf(dec, "%d", decimalVal);
    // combine file name, ext, and decimal value
    strcpy(file, newFile);
    strcat(file, ext);
    strcat(file, ".");
    strcat(file, dec);
}
void getNormalFile(char *file)
{
    if (!strcmp(file, ".") || !strcmp(file, ".."))
        return;
    char finalFile[1000];
    memset(finalFile, 0, sizeof(finalFile));
    char *token = strtok(file, "/");
    while (token != NULL)
    {
        printf("%s\n", token);
        char ext[1000], dec[1000], newFile[1000];
        memset(ext, 0, sizeof(ext));
        memset(dec, 0, sizeof(dec));
        memset(newFile, 0, sizeof(newFile));
        int nExt = 0, nFile = 0, nDec = 0, flag = 0;
        // get file name, ext, and decimal
        for (int i = strlen(token) - 1; i >= 0; i--)
        {
            if (token[i] == '.' && flag == 0)
            {
                // dec[nDec++] = file[i];
                flag = 1;
                continue;
            }
            if (token[i] == '.' && flag == 1)
            {
                ext[nExt++] = token[i];
                flag = 2;
                continue;
            }
            if (flag == 1)
                ext[nExt++] = token[i];
            else if (flag == 2)
                newFile[nFile++] = token[i];
            else
                dec[nDec++] = token[i];
        }
        revstr(dec);
        revstr(ext);
        revstr(newFile);
        if (!strcmp(newFile, ""))
        {
            strcpy(newFile, ext);
            memset(ext, 0, sizeof(ext));
        }
        printf("%s <-> %s <-> %s\n", dec, ext, newFile);
        int decVal = atoi(dec);
        // get binary code
        int nBin = 0;
        char binary[1000];
        memset(binary, 0, sizeof(binary));
        while (decVal > 0)
        {
            binary[nBin++] = (char)(48 + decVal % 2);
            decVal /= 2;
        }
        revstr(binary);
        printf("%ld -> ", strlen(binary));
        for (int i = 0; i < strlen(binary); i++)
            printf("%c", binary[i]);
        printf("\n");
        // update the file name
        for (int i = 0; i < nBin; i++)
        {
            if (binary[i] == '1')
            {
                newFile[i] += 32;
            }
        }

        // combine file name, ext
        printf("%s <> %s <> %s\n", ext, newFile, binary);
        strcat(newFile, ext);
        strcat(finalFile, "/");
        strcat(finalFile, newFile);
        token = strtok(NULL, "/");
    }
    strcpy(file, finalFile);
}
static int xmp_getattr(const char *path, struct stat *stbuf)
{
    printf("path getattr %s\n", path);
    char *mode1 = strstr(path, pref1);
    // path = /Animeku_hebat/anya_FORGER.txt
    // mode1 = _hebat/anya_FORGER.txt
    // temp = anya_FORGER.txt
    if (mode1 != NULL)
    {
        mode1 += strlen(pref1);
        printf("1 %s\n", mode1);
        char *temp = strchr(mode1, '/');
        if (temp)
        {
            temp += 1;
            printf(">1 %s\n", temp);
            atbashRot13(temp);
            printf("<1 %s\n", path);
        }
    }

    char *mode2 = strstr(path, pref2);
    if (mode2 != NULL)
    {
        mode2 += strlen(pref2);
        printf("2 %s\n", mode2);
        char *temp = strchr(mode2, '/');
        if (temp)
        {
            temp += 1;
            printf(">2 %s\n", temp);
            decodeVigenere(temp);
            printf("<2 %s\n", path);
        }
    }
    char *mode3 = strstr(path, pref3);
    if (mode3 != NULL)
    {
        mode3 += strlen(pref3);
        printf("3 %s\n", mode3);
        char *temp = strchr(mode3, '/');
        if (temp)
        {
            printf(">2 %s\n", temp);
            getNormalFile(temp);
            printf("<2 %s\n", path);
        }
    }
    char newFullPath[1000];
    if (strcmp(path, "/") == 0)
    {
        path = dirPath;
        sprintf(newFullPath, "%s", path);
    }
    else
        sprintf(newFullPath, "%s%s", dirPath, path);
    if (lstat(newFullPath, stbuf) == -1)
        return -errno;
    return 0;
}
static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
    printf("path readdir %s\n", path);
    char *mode1 = strstr(path, pref1);
    if (mode1 != NULL)
    {
        mode1 += strlen(pref1);
        printf("1 %s\n", mode1);
        char *temp = strchr(mode1, '/');
        if (temp)
        {
            temp += 1;
            printf(">1 %s\n", temp);
            atbashRot13(temp);
            printf("<1 %s\n", path);
        }
    }
    char *mode2 = strstr(path, pref2);
    if (mode2 != NULL)
    {
        mode2 += strlen(pref2);
        printf("2 %s\n", mode2);
        char *temp = strchr(mode2, '/');
        if (temp)
        {
            temp += 1;
            printf(">2 %s\n", temp);
            decodeVigenere(temp);
            printf("<2 %s\n", path);
        }
    }
    char *mode3 = strstr(path, pref3);
    if (mode3 != NULL)
    {
        mode3 += strlen(pref3);
        printf("3 %s\n", mode3);
        char *temp = strchr(mode3, '/');
        if (temp)
        {
            printf(">3 %s\n", temp);
            getNormalFile(temp);
            printf("<3 %s\n", path);
        }
    }
    char newFullPath[1000];
    if (strcmp(path, "/") == 0)
    {
        path = dirPath;
        sprintf(newFullPath, "%s", path);
    }
    else
        sprintf(newFullPath, "%s%s", dirPath, path);
    DIR *folder;
    struct dirent *de;
    (void)offset;
    (void)fi;
    folder = opendir(newFullPath);
    if (folder == NULL)
        return -errno;
    while ((de = readdir(folder)) != NULL)
    {
        if (!strcmp(de->d_name, ".") || !strcmp(de->d_name, ".."))
            continue;
        struct stat st;
        memset(&st, 0, sizeof(st));
        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;
        if (mode1 != NULL)
        {
            atbashRot13(de->d_name);
            printf("readdir rename %s\n", de->d_name);
        }
        if (mode2 != NULL)
        {
            encodeVigenere(de->d_name);
            printf("readdir rename %s\n", de->d_name);
        }
        if (mode3 != NULL)
        {
            getSpecialFile(de->d_name);
            printf("readdir rename %ld %s\n", strlen(de->d_name), de->d_name);
        }
        if (filler(buf, de->d_name, &st, 0))
            break;
    }
    closedir(folder);
    return 0;
}
static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    printf("path read %s\n", path);
    char *mode1 = strstr(path, pref1);
    if (mode1 != NULL)
    {
        mode1 += strlen(pref1);
        printf("1 %s\n", mode1);
        char *temp = strchr(mode1, '/');
        if (temp)
        {
            temp += 1;
            printf(">1 %s\n", temp);
            atbashRot13(temp);
            printf("<1 %s\n", path);
        }
    }
    char *mode2 = strstr(path, pref2);
    if (mode2 != NULL)
    {
        mode2 += strlen(pref2);
        printf("2 %s\n", mode2);
        char *temp = strchr(mode2, '/');
        if (temp)
        {
            temp += 1;
            printf(">2 %s\n", temp);
            decodeVigenere(temp);
            printf("<2 %s\n", path);
        }
    }
    char *mode3 = strstr(path, pref3);
    if (mode3 != NULL)
    {
        mode3 += strlen(pref3);
        printf("3 %s\n", mode3);
        char *temp = strchr(mode3, '/');
        if (temp)
        {
            temp += 1;
            printf(">3 %s\n", temp);
            getNormalFile(temp);
            printf("<3 %s\n", path);
        }
    }
    char newFullPath[1000];
    if (strcmp(path, "/") == 0)
    {
        path = dirPath;
        sprintf(newFullPath, "%s", path);
    }
    else
        sprintf(newFullPath, "%s%s", dirPath, path);
    int fd;
    int res;
    (void)fi;
    fd = open(newFullPath, O_RDONLY);
    if (fd == -1)
        return -errno;
    res = pread(fd, buf, size, offset);
    if (res == -1)
        res = -errno;

    writeLog2("READ", path, "");

    close(fd);
    return res;
}
static int xmp_mkdir(const char *path, mode_t mode)
{
    printf("path mkdir %s\n", path);
    char *mode1 = strstr(path, pref1);
    if (mode1 != NULL)
    {
        mode1 += strlen(pref1);
        printf("1 %s\n", mode1);
        char *temp = strchr(mode1, '/');
        if (temp)
        {
            temp += 1;
            printf(">1 %s\n", temp);
            atbashRot13(temp);
            printf("<1 %s\n", path);
        }
    }
    char *mode2 = strstr(path, pref2);
    if (mode2 != NULL)
    {
        mode2 += strlen(pref2);
        printf("2 %s\n", mode2);
        char *temp = strchr(mode2, '/');
        if (temp)
        {
            temp += 1;
            printf(">2 %s\n", temp);
            decodeVigenere(temp);
            printf("<2 %s\n", path);
        }
    }
    char *mode3 = strstr(path, pref3);
    if (mode3 != NULL)
    {
        mode3 += strlen(pref3);
        printf("3 %s\n", mode3);
        char *temp = strchr(mode3, '/');
        if (temp)
        {
            temp += 1;
            printf(">3 %s\n", temp);
            getNormalFile(temp);
            printf("<3 %s\n", path);
        }
    }
    char newFullPath[1000];
    if (strcmp(path, "/") == 0)
    {
        path = dirPath;
        sprintf(newFullPath, "%s", path);
    }
    else
    {
        sprintf(newFullPath, "%s%s", dirPath, path);
    }

    int res;
    res = mkdir(newFullPath, mode);
    if (res == -1)
        return -errno;

    writeLog2("MKDIR", path, "");

    return 0;
}
static int xmp_rmdir(const char *path)
{
    printf("path rmdir %s\n", path);
    char *mode1 = strstr(path, pref1);
    if (mode1 != NULL)
    {
        mode1 += strlen(pref1);
        printf("1 %s\n", mode1);
        char *temp = strchr(mode1, '/');
        if (temp)
        {
            temp += 1;
            printf(">1 %s\n", temp);
            atbashRot13(temp);
            printf("<1 %s\n", path);
        }
    }
    char *mode2 = strstr(path, pref2);
    if (mode2 != NULL)
    {
        mode2 += strlen(pref2);
        printf("2 %s\n", mode2);
        char *temp = strchr(mode2, '/');
        if (temp)
        {
            temp += 1;
            printf(">2 %s\n", temp);
            decodeVigenere(temp);
            printf("<2 %s\n", path);
        }
    }
    char *mode3 = strstr(path, pref3);
    if (mode3 != NULL)
    {
        mode3 += strlen(pref3);
        printf("3 %s\n", mode3);
        char *temp = strchr(mode3, '/');
        if (temp)
        {
            temp += 1;
            printf(">3 %s\n", temp);
            getNormalFile(temp);
            printf("<3 %s\n", path);
        }
    }
    char newFullPath[1000];
    if (strcmp(path, "/") == 0)
    {
        path = dirPath;
        sprintf(newFullPath, "%s", path);
    }
    else
    {
        sprintf(newFullPath, "%s%s", dirPath, path);
    }

    int res;
    res = rmdir(newFullPath);
    if (res == -1)
        return -errno;
    writeLog2("RMDIR", path, "");

    return 0;
}
static int xmp_unlink(const char *path)
{
    printf("path unlink %s\n", path);
    char *mode1 = strstr(path, pref1);
    if (mode1 != NULL)
    {
        mode1 += strlen(pref1);
        printf("1 %s\n", mode1);
        char *temp = strchr(mode1, '/');
        if (temp)
        {
            temp += 1;
            printf(">1 %s\n", temp);
            atbashRot13(temp);
            printf("<1 %s\n", path);
        }
    }
    char *mode2 = strstr(path, pref2);
    if (mode2 != NULL)
    {
        mode2 += strlen(pref2);
        printf("2 %s\n", mode2);
        char *temp = strchr(mode2, '/');
        if (temp)
        {
            temp += 1;
            printf(">2 %s\n", temp);
            decodeVigenere(temp);
            printf("<2 %s\n", path);
        }
    }
    char *mode3 = strstr(path, pref3);
    if (mode3 != NULL)
    {
        mode3 += strlen(pref3);
        printf("3 %s\n", mode3);
        char *temp = strchr(mode3, '/');
        if (temp)
        {
            temp += 1;
            printf(">3 %s\n", temp);
            getNormalFile(temp);
            printf("<3 %s\n", path);
        }
    }
    char newFullPath[1000];
    if (strcmp(path, "/") == 0)
    {
        path = dirPath;
        sprintf(newFullPath, "%s", path);
    }
    else
    {
        sprintf(newFullPath, "%s%s", dirPath, path);
    }
    int res;
    res = unlink(newFullPath);
    if (res == -1)
        return -errno;
    writeLog2("UNLINK", path, "");

    return 0;
}

static int xmp_rename(const char *old, const char *new)
{

    int res;
    char oldPath[1000], newPath[1000];

    sprintf(oldPath, "%s%s", dirPath, old);
    sprintf(newPath, "%s%s", dirPath, new);
    res = rename(oldPath, newPath);
    if (res == -1)
        return -errno;

    char *oldmode1 = strstr(oldPath, pref1);
    char *newmode1 = strstr(newPath, pref1);
    if (oldmode1 != NULL && newmode1 == NULL)
        writeLog1("RENAME", "terdecode", oldPath, newPath);
    if (oldmode1 == NULL && newmode1 != NULL)
        writeLog1("RENAME", "terenkripsi", oldPath, newPath);

    writeLog2("RENAME", old, new);

    return 0;
}
static int xmp_open(const char *path, struct fuse_file_info *fi)
{
    printf("path open %s\n", path);
    char *mode1 = strstr(path, pref1);
    if (mode1 != NULL)
    {
        mode1 += strlen(pref1);
        printf("1 %s\n", mode1);
        char *temp = strchr(mode1, '/');
        if (temp)
        {
            temp += 1;
            printf(">1 %s\n", temp);
            atbashRot13(temp);
            printf("<1 %s\n", path);
        }
    }
    char *mode2 = strstr(path, pref2);
    if (mode2 != NULL)
    {
        mode2 += strlen(pref2);
        printf("2 %s\n", mode2);
        char *temp = strchr(mode2, '/');
        if (temp)
        {
            temp += 1;
            printf(">2 %s\n", temp);
            decodeVigenere(temp);
            printf("<2 %s\n", path);
        }
    }
    char *mode3 = strstr(path, pref3);
    if (mode3 != NULL)
    {
        mode3 += strlen(pref3);
        printf("3 %s\n", mode3);
        char *temp = strchr(mode3, '/');
        if (temp)
        {
            temp += 1;
            printf(">3 %s\n", temp);
            getNormalFile(temp);
            printf("<3 %s\n", path);
        }
    }
    char newFullPath[1000];
    if (strcmp(path, "/") == 0)
    {
        path = dirPath;
        sprintf(newFullPath, "%s", path);
    }
    else
    {
        sprintf(newFullPath, "%s%s", dirPath, path);
    }
    int fd;
    fd = open(newFullPath, fi->flags);
    if (fd == -1)
        return -errno;

    writeLog2("OPEN", path, "");

    close(fd);
    return 0;
}
static int xmp_write(const char *path, const char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    printf("path write %s\n", path);
    char *mode1 = strstr(path, pref1);
    if (mode1 != NULL)
    {
        mode1 += strlen(pref1);
        printf("1 %s\n", mode1);
        char *temp = strchr(mode1, '/');
        if (temp)
        {
            temp += 1;
            printf(">1 %s\n", temp);
            atbashRot13(temp);
            printf("<1 %s\n", path);
        }
    }
    char *mode2 = strstr(path, pref2);
    if (mode2 != NULL)
    {
        mode2 += strlen(pref2);
        printf("2 %s\n", mode2);
        char *temp = strchr(mode2, '/');
        if (temp)
        {
            temp += 1;
            printf(">2 %s\n", temp);
            decodeVigenere(temp);
            printf("<2 %s\n", path);
        }
    }
    char *mode3 = strstr(path, pref3);
    if (mode3 != NULL)
    {
        mode3 += strlen(pref3);
        printf("3 %s\n", mode3);
        char *temp = strchr(mode3, '/');
        if (temp)
        {
            temp += 1;
            printf(">3 %s\n", temp);
            getNormalFile(temp);
            printf("<3 %s\n", path);
        }
    }
    char newFullPath[1000];
    if (strcmp(path, "/") == 0)
    {
        path = dirPath;
        sprintf(newFullPath, "%s", path);
    }
    else
    {
        sprintf(newFullPath, "%s%s", dirPath, path);
    }
    (void)fi;
    int fd, res;
    fd = open(newFullPath, O_WRONLY);
    if (fd == -1)
        return -errno;
    res = pwrite(fd, buf, size, offset);
    if (res == -1)
        res = -errno;

    writeLog2("WRITE", path, "");
    close(fd);
    return res;
}

static int xmp_create(const char *path, mode_t mode, struct fuse_file_info *fi)
{
    printf("path create %s\n", path);
    char *mode1 = strstr(path, pref1);
    if (mode1 != NULL)
    {
        mode1 += strlen(pref1);
        printf("1 %s\n", mode1);
        char *temp = strchr(mode1, '/');
        if (temp)
        {
            temp += 1;
            printf(">1 %s\n", temp);
            atbashRot13(temp);
            printf("<1 %s\n", path);
        }
    }
    char *mode2 = strstr(path, pref2);
    if (mode2 != NULL)
    {
        mode2 += strlen(pref2);
        printf("2 %s\n", mode2);
        char *temp = strchr(mode2, '/');
        if (temp)
        {
            temp += 1;
            printf(">2 %s\n", temp);
            decodeVigenere(temp);
            printf("<2 %s\n", path);
        }
    }
    char *mode3 = strstr(path, pref3);
    if (mode3 != NULL)
    {
        mode3 += strlen(pref3);
        printf("3 %s\n", mode3);
        char *temp = strchr(mode3, '/');
        if (temp)
        {
            temp += 1;
            printf(">3 %s\n", temp);
            getNormalFile(temp);
            printf("<3 %s\n", path);
        }
    }
    char newFullPath[1000];
    if (strcmp(path, "/") == 0)
    {
        path = dirPath;
        sprintf(newFullPath, "%s", path);
    }
    else
    {
        sprintf(newFullPath, "%s%s", dirPath, path);
    }

    int res;
    res = creat(newFullPath, mode);
    if (res == -1)
        return -errno;

    writeLog2("CREATE", path, "");

    return 0;
}
static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,
    .readdir = xmp_readdir,
    .read = xmp_read,
    .mkdir = xmp_mkdir,
    .unlink = xmp_unlink,
    .rmdir = xmp_rmdir,
    .rename = xmp_rename,
    .open = xmp_open,
    .write = xmp_write,
    .create = xmp_create,
};
int main(int argc, char *argv[])
{
    umask(0);
    return fuse_main(argc, argv, &xmp_oper, NULL);
}
